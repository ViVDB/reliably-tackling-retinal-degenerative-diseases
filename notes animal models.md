# sources

1) A common microRNA signature in mouse models of retinal degeneration → [miRNA](https://www.sciencedirect.com/science/article/pii/S0014483508002923#bib13)
2) Immunological Considerations for Retinal Stem Cell Therapy → [immune1](https://link.springer.com/chapter/10.1007/978-3-030-28471-8_4)
3) Cytokine interplay among the diseased retina, inflammatory cells and mesenchymal stem cells - a clue to stem cell-based therapy → [immune2](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC6851013/)
4) Development of Stem Cell Therapies for RetinalDegeneration → [reviewTherapy1](https://sci-hub.se/https://cshperspectives.cshlp.org/content/12/8/a035683.short)
5) [materials]
6) Biology and therapy of inherited retinal degenerative disease:insights from mouse models [veleri2015biology]
7) Retinitis pigmentosa: rod photoreceptor rescue by a calcium-channel blocker in the rdmouse [frasson1999retinitis]
8) Functional and structural modifications during retinal degeneration in the rd10 mouse [TUNEL](https://www.sciencedirect.com/science/article/pii/S0306452208008993)
9) Mouse Models of NMNAT1-Leber Congenital Amaurosis (LCA9) Recapitulate Key Features of the Human Disease [newMutant]
10) Animal models of age related macular degeneration [animalModels](https://doi.org/10.1016/j.mam.2012.06.003)
11) Optical Coherence Tomography of Animal Models of Retinitis Pigmentosa: From Animal Studies to Clinical Applications [tomography](https://www.hindawi.com/journals/bmri/2019/8276140/)
12) Cellular Responses to Photoreceptor Death in the rd1 Mouse Model of Retinal Degeneration [ish](https://iovs.arvojournals.org/article.aspx?articleid=2124946)
13) Donor and host photoreceptors engage in material transfer following transplantation of post-mitotic photoreceptor precursors [eyeCulture](https://www.nature.com/articles/ncomms13029#Sec12)
14) Differentiation and Transplantation of Embryonic Stem Cell-Derived Cone Photoreceptors into a Mouse Model of End-Stage Retinal Degeneration [coneDegen](https://www.sciencedirect.com/science/article/pii/S2213671117301844?via%3Dihub)
15) Medium- to long-term survival and functional examination of human iPSC-derived retinas in rat and primate models of retinal degeneration [laser](https://www.sciencedirect.com/science/article/pii/S2352396418305322?via%3Dihub)
17) iPSC-Derived Retina Transplants Improve Vision in rd1 End-Stage Retinal-Degeneration Mice [shuttle](https://www.sciencedirect.com/science/article/pii/S2213671116302983#sec4)
18) In Vivo Imaging of Human Retinal Flow Dynamics by Color Doppler Optical Coherence Tomography [bloodFlow](https://jamanetwork.com/journals/jamaophthalmology/article-abstract/415121)
19) In Vivo Imaging of Microscopic Structures in the Rat Retina [inVivoGFP](https://iovs.arvojournals.org/article.aspx?articleid=2185434)
20) Fluorescein Labeled Leukocytes for in vivo Imaging of Retinal Vascular Inflammation and Infiltrating Leukocytes in Laser-Induced Choroidal Neovascularization Model [inVivoLeukocytes](https://www.tandfonline.com/doi/10.1080/09273948.2018.1429637)
21) In vivo imaging of adeno-associated viral vector labelled retinal ganglion cells [viralGFP](https://www.nature.com/articles/s41598-018-19969-9)
22) In Vivo Imaging of Retinal Hypoxia Using HYPOX-4-Dependent Fluorescence in a Mouse Model of Laser-Induced Retinal Vein Occlusion (RVO) [fluorescein](https://iovs.arvojournals.org/article.aspx?articleid=2646756#166907875)
23) High-resolution, in vivo multimodal photoacoustic microscopy, optical coherence tomography, and fluorescence microscopy imaging of rabbit retinal neovascularization [photoacoustic](https://www.nature.com/articles/s41377-018-0093-y#Sec2)

# model validation

## types of models

* RPE loss [reviewTherapy1]
* photoreceptor loss [reviewTherapy1]
* combinations [reviewTherapy1]

## model limitations

mice don't have a macula (cone-rich region) so cone studies need to be adapted [reviewTherapy1,veleri2015biology] [animalModels]

much less is known about cone degeneration, especially when it happens before rods [coneDegen]

(they are however easier to use -- [veleri2015biology] [animalModels])

## model validation

need to record symptoms to be able to compare them to diseases and other models [newMutant]

includes cell death timeline, electric response,immunohistochemistry,general cell and retina morphology... [TUNEL, newMutant]

## inherited

* basically, change genes until you get similar symptoms in mouse than in humans [miRNA]
* those gene modifications are similar to ones found in humans (some of the human mutations at least) [miRNA]
* some gene modifications are bad models (e.g. debris accumulation can be "healed" by any kind of cell injection, and not same long-term symptoms than most retinal degenerative diseases) [reviewTherapy1]
* sometimes mutations naturally occuring (see 'Rescuing End-Stage Retinal Degeneration' in [reviewTherapy1])
    * → genetic control of known important genes pre-study and "batch id" to be able to identify if when a problem is found the study is affected



## age-related

complex process involving both genetic and environmental factors [animalModels]

several symptoms, need to consider the important ones [animalModels]
can't have all of them at once [animalModels]
can have symptoms that shouldn't be there [animalModels]
transgenic mouse lines [animalModels]
    → basically we are just trying to treat symptoms and not causes

there are other animals [animalModels]
    → but they are not practical for large scale testing at least

laser induced → wet AMD [animalModels]
laser induced [laser]
accelerated senescence[animalModels]
high fat/sugar diet[animalModels]

## immune system

* cell transplantation is followed by immune response, notamment pour RPE or by microglia. This was in mice and other animals, and there are ways to reduce immune response (immunodeficiency or treatments)[immune1]
    * → immunodeficient mice should not be used, except for human cell transplant, and human cell transplant should be tested on in vitro devices in priority
    * → keep track of immunosuppression treatments
* Inserting cells into diseased eyes changes cytokines activity [immune2]
    * → important to follow

# monitoring

## quantitative RT PCR = PCR + microarray of RNA strands or PCR + fluorescent dyes

mouse killed → sample → RT-PCR [miRNA, ish]

does not give you the architecture, unlike staining

## Staining

mouse killed → sample fixed → staining → microscopy [miRNA, materials, frasson1999retinitis]

can be for specific protein detection -- key proteins [materials, TUNEL, newMutant]

can be for DNA/RNA detection -- in situ hybridization [ish]


## in vivo staining

Leukocytes → immune response [inVivoLeukocytes]
hypoxia staining [fluorescein]
fluorescent microscopy → stain blood... [photoacoustic]

advantage of many more observations

## GFP expression

* label specific genes with GFP [reviewTherapy1,eyeCulture]
* GFP isn't the same as injected protein there, as there is proteic material sharing [reviewTherapy1,eyeCulture]

possible to extract eyes then culture them for pseudo "in vivo" observations... [eyeCulture] no in vivo actual observation


possible to use in vivo [inVivoGFP]
probably possible to add fluorescence in any mouse [viralGFP]
    would probably need validation by immunohistochemistry

## Spectral  domain-optical  coherence tomography

* tomography → in vivo structures → 2µm resolution [materials, newMutant] [tomography]
  * → automated image analysis? **TODO**
  * "Retinal thickness was measured as the distance between the nerve fibre layer and the hyporeflective boundary between the retinal pigment epithelium (RPE) and choroid."[newMutant]
* possible to get blood flows [bloodFlow]
* just a slice scanned, and artefacts [photoacoustic]

quantum dot tomography?

## optokinetic head tracking

* see if the mouse can follow movements [reviewTherapy1]

## electroretinogram

* electrodes in the eye → can track rods and neurons [veleri2015biology, frasson1999retinitis][laser]

* possible recording without actually light perception → graft/host not connected [laser]

## TUNEL-labelling

possible to see where are apoptotic cells by detecting breaking DNA (in dead mice) [TUNEL][ish]

## individual cell electric recording

retinal slices → perfused → electrical stimulus and measured response [TUNEL]

## in vivo retinal imaging

* "Micron retinal imaging microscope system" → retina pictures [newMutant]
    * → no automation, low sample size anyways

## light and electron microscopy

[newMutant] [laser]

IR-DIC may be used to determine the prevalence of some molecules [laser]

## pupillary light response

[newMutant]

## fundus autofluorescence

fundus autofluorescence indicates RPE thickness

can be used on mice [animalModels]

## Multi-electrode array (MEA) recordings

mouse killed and retina cultured → electric recordings [laser]

light stimulation is detected [laser]

## shuttle avoidance system test, water maze

[animalModels]
[shuttle]

## Fluorescein angiography

[fluorescein][photoacoustic]
only blood

## photoacoustic microscopy

[photoacoustic]
only blood vessels, again


# Tracking

## operation procedures

* keep track of disturbances of the outer limiting membrane since it can improve cell migration from sub-retinal injection to retina [review][reviewTherapy1]

## confounding factors

* material transfer between host and implanted cells [reviewTherapy1] "Thus, to ascertain whether there is restoration of vision because of the connection of hPSC-derived photoreceptors with the host retina, it is necessary to utilize end-stage models of retinal degeneration, with no remaining endogenous photoreceptors. In addition, the host retinal environment present in late-stage retinal degeneration models may be more in keeping with potential clinical applications that are likely to involve transplantation in regions of the retina with very few remaining photoreceptors"
    * keep track of which confounding factors have been taken into account

## involved systems

immune  (see immune)
    oxidative stress [animalModels]
    inflammation [animalModels]
lipid metabolism [animalModels]
carbohydrate metabolism [animalModels]
