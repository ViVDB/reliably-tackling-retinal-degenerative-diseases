This is a work exploring the research question: "How  can retinal degenerative diseases be tackled?".

The actual work is available in the pdf: [auxiliary/doc.pdf](./auxiliary/doc.pdf). In addition, [auxiliary/ref.pdf](./auxiliary/ref.pdf) contains the references for the related oral presentation. The oral presentation itself is not included here for copyright reasons. The single picture in the work is under creative commons licence by the authors, so it can be re-used but the authors, the source and the changes made need to be cited, basically (Commons Attribution 4.0 International License http://creativecommons.org/licenses/by/4.0/).

-----

Copyright 2020 Vincent Vandenbroucke

This work may be distributed and/or modified under the
conditions of the LaTeX Project Public License, either version 1.3
of this license or (at your option) any later version.
The latest version of this license is in
http://www.latex-project.org/lppl.txt
and version 1.3 or later is part of all distributions of LaTeX
version 2005/12/01 or later.

This work is finished and not maintained.

This work consists of the files:
* [README.md](./README.md)
* [ref.tex](./ref.tex) (references for the oral presentation)
* [text.tex](./text.tex)
* [doc.tex](./doc.tex)
* [bib.bib](./bib.bib)
* [further.bib](./further.bib)
* [additional_intro.bib](./research/other_ref/additional_intro_material/additional_intro.bib)
* [mouseModel.bib](./research/mouse_model/mouseModel.bib)
* [in_vitro.bib](./research/in_vitro/in_vitro.bib)
* [in_silico.bib](./research/in_silico/in_silico.bib)


The compiled work consists of the files:
* [doc.pdf](./auxiliary/doc.pdf)
* [ref.pdf](./auxiliary/ref.pdf)

Files in the figure directory are property of ULiège for the logo and a copy of the image found in the compiled work, see the note above. Other markdown files are draft notes and may not be copied or distributed, they are not under LPPL.

The makefile and the work is derived from the template that may be found here: https://gitlab.com/ViVDB/latex-template, thus the makefile is under the license from there, it is not a part of this work.
