# retina in vitro

would allow to determine toxicity more easily [organochip + review] → iPSC to cancer...

## explants

life relatively short [organochip]

limited availability but more and more used [dishModel]

possible to culture tissue slices in microfluidics [beginMicro]

explants used for immune response analysis [inflamed]

there are pig and bovine explants available for study -- cell damage can be induced through hypoxia and toxic agents, they survive 7-14 d. They lack RPE and brain connections. They have other problems but still better than single cell culture [pigEye]

could be used in pair comparison [pigEye]

could be cultured better in microfluidics devices [pigEye]

RPE and choroid culture possible in vascularized devices [pigEye]

there have been attempts at whole eye culture [pigEye] (cats were judged unethical for some reason so now reboot with pigs and cows)

## cell cultures

immortalized cell lines "do not necessarily exhibit characteristics of the native tissue." [dishModel]

established protocols for differentiation in RPE, ganglion cells [dishModel]

culture of RPE may be very interesting to study some aspects of AMD as AMD primarily takes place there [pigEye]

ganglion cells monolayer have been used to study glaucoma, but organ cultures would be better [pigEye]

there are the general problems associated with cell culture [pigEye] (contamination, phenotype change, karyotype abnormalities and so on)

plus cell source may be a problem (for not induced) → fetal donors, dead donors... [pigEye]

primary ganglion cells are hard to get in quantity [pigEye]

primary microglia can be gotten from the brain, is different in infants, and needs to be carefully monitored for activation [pigEye]

Müller cells from fish and bird can transform into neurons → same can be achieved in vitro for humans [pigEye]

induced stem cells can be much more abundant [pigEye]

## organoids

Drawbacks
"
 (i) the functional maturation of differentiated cells,
 (ii) lack of essential cell types (e.g. microglia), [dishModel] (and few RPE [dishModel]
 (iii) lack of a physiological interplay of the various retinal cell types especially of photoreceptors and retinal pigment epithelia (RPE) [dishModel]
 (iv) a missing vascularization (reviewed in Achberger et al., 2019; Yin et al., 2016).
 "
 [organochip]

initially wasteful inefficient procedures → manually separate good and bad organoids [cells]

low reproducibility before [milliwell]

[milliwell] seems much better than alternatives

cell immaturity may be a problem for disease modelling [dishModel] ([milliwell] may improve)

little presence of RPE [dishModel]

## microfluidics

cell types made individually, not possible to make full retina, only partial layers [organochip]

there are interfaces between cell types to allow communication, but hard (still possible) to make multiple. Also hinders imaging. and there is a physical barrier between cells[chipScaling]

there are devices in which microchannels link tissues, but small interface area overall [chipScaling]

microgrooves under the device are better for connectivity [chipScaling]

electric conductivity measurements for barrier monitoring and electrodes to monitor neuronal activity [chipScaling]

integration of nano-3DP may be cool

Direct contact is a priori not possible [chipScaling]

other devices have direct contact so that cell migration and gradients are observable [gradient]

"A way to overcome these obstacles is the culture of retinal explants on a microchannel system, which holds the retina in place by negative pressure" [pigEye]

now possible to make blood vessels in vitro [bloodVessels]

basically there are a few simple devices that allow partial reproduction of retina or Blood-retinal barrier [expertOpinion]

## organochip

add RPE and organoids separately [organochip]

no light sensitivity test possible [organochip]

"Yet, ROs lack vascularization and cannot recapitulate the important physiological interactions of matured photoreceptors and the retinal pigment epithelium (RPE)." [organochip]

# disease model

limited attempts at proving iPSC differentiated into rods (only) from RP patients can be improved by vitamins -- model very different from reality though [autologousModel]

results to help drug screening from PSCs of given gene background donors (for simple hereditary diseases)[dishModel]

"Furthermore, genome editing methods, such as the CRISPR/Cas system, can be used to generate isogenic hPSC lines with reduced background genetic variability [14]. Such lines can help to elucidate downstream changes in gene expression caused by the disease-associated mutation [15,16], and also reveal the influence of a specific genetic variant on cellular phenotype [17]. This approach has already been validated for macular dystrophies [11,18]." [dishModel]

co-cultures for 10 y already to model cell interactions [dishModel]
"Microfluidic co-cultures approaches have already been successfully trialled, combining the immortalized human RPE cell line ARPE19 with the human umbilical vein endothelial cell line (HUVEC), as a way to mimic choroidal angiogenesis [dishModel]."

cultures to model specific aspects of AMD and glaucoma (see culture) [pigEye]

microfluidics devices stay simpler than their natural counterparts [pigEye]

# monitoring

## immunostaining

to determine the different cell types and their structure [organochip]

## single cell transcriptomics

may be very useful for scaled quality assessment [scRNA]

can identify cell types based on an atlas of transcriptome [atlas]

can be used to identify key signals and keep cells as close to what happens in vivo as possible [signaling]

→ could be useful for semi-automatic monitoring and fine characterization of cell types (need extracted cells first though, not on line)
→ could be useful for mechanism identification when disease model (or for all models I guess)

"Therefore, any use of cell lines for experimental research must include a validation of its origins and characteristic properties." [pigEye] → contamination/wrong identification must be monitored as well -- only valid for cell lines, I guess, not induced pluripotent cells

## GFP tracking

in [milliwell]

## microscopy

on extracted small parts in [milliwell]

two-photon as well [pigEye]

# reliability and scalability

[organochip] and [milliwell] together are potentially scalable and reliable, but still problem of cell source
potentially could be done remotely → directly buy organoids, disposable microfluidics devices
(when cells banks are available)
(potentially even recreate singular mutations in vitro)

serum is still widespread [pigEye]

## cell source

iPSCs for drug testing on rods [autologousModel]

derived from donors [organochip]

(mouse) embryonic stem cells [milliwell]

## immune system

lacking [organochip, dishModel]
