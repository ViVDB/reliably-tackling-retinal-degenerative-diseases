# in vivo

in vivo tracking → quantum dots? validated in vivo dyes?

standardized practice

# in vitro

merge [milliwell] and [organochip]

add missing cell types

standardized scalable device
* "quality by design"
* "cost by design"
* "scalability by design"

simple disease modelling for reliable proof of concept for more

# in silico

in silico model integration → bare-bone modular model

model refining based on devices
