doc.pdf:doc.tex text.tex bib.bib
	# no prerequisite is added above because latexmk also checks if stuff
	# changed before running
	#
	# a priori luatex is expected
	# may need to use the shell-escape line if some packages are used
	# (minted, probably tikz, and probably others)
	latexmk --output-directory=./auxiliary -lualatex doc.tex
	# latexmk --output-directory=./auxiliary -shell-escape -lualatex doc.tex
