"Although retinal and optic nerve diseases are generally well characterized clinically, the molecular events underlying pathologies often remain poorly understood. Age-related macular degeneration (AMD) exemplifies this problem: despite international efforts to understand its pathogenesis, there is still no effective treatment or cure for the non-exudative, (atrophic or dry) form of AMD." [dishModel]

many important aspects of AMD have already been revealed [animalModels]

# sequencing to get gene targets

gene discovery [2005RNA1]

piRNA (non-coding RNA) discovery [2018piRNA]

miRNA discovery in sick mice [2019miRNA]

linkage and DNA analysis to get gene responsible from disease [2019linkage]

# useful phenomenological modelling

for now empirical parameter (growth factors) optimization based on sc-RNA-seq [signaling] 2019

# mechanistic modelling

apply differential equations to ganglion cell generation/degeneration [1978earlyDifferential]

apply differential equations to oxygen diffusion to fit data and determine oxygen used, to see if it explained a hereditary degeneration [2006cat]

**study link phenotype-environment-genotype (yet poorly understood)** [2010angiogenesis] can't be used, only abstract available

*possible model molecular transport in microfluidics devices [gradient] 2015*

model rods and cones by unit cells with single energy pool to determine what causes cone death in rod dominated RP [2016quantifyingRP]

2016 [mathModels]
    * oxygen diffusion through retinal layers
    * neuroglobin oxygen transport model
    * pressure drop in choriocapillaris (simplified) (little experimental data)
    * individual rod day-night cycle
    * capillary development
    * studies on formation of ganglion cells and photoreceptors -- both mechanistic and phenomenological. They usesingle  simple principles, that can explain several features, not how things go together [salbreux2012coupling, jiao2014avian, barton2015retinogenesis]
* disease modelling:
    * trophic factor made by rods for cones, ODE, explains some observations on cones and rods
    * toxic factor by dying rod for cones, ODE+stochastic, explains some observations
    * oxygen toxicity, partial ODE, explains antioxidant effects
    * choriocapillaris neo-vascularization, weird  stochastic model, relatively good agreement with data

eye drop diffusion modelling through the eye up till the retina [2017eyeDrop]

cholesterol metabolism potentially starting AMD (drusen kinetics) [2017cholesterol]

VEGF metabolism by RPE cells to study wet AMD progression [2017AMD]

"Strain-triggered mechanical feedback in self-organizing optic-cup morphogenesis" [2018strain]

partial differential equations for photoreceptor degeneration under hyperoxia or genetic problems solved with finite elements [2018predictiveDifferential] → model explains some patterns observed, not all

finite elements for oxygen concentrations starting AMD (drusen limits O2 diffusion) (2019) [finiteElement]

intravitreal protein delivery for delivery to the retina → diffusion mechanisms [2019intravitreal]

simulation of a single photoreceptor metabolism through a bunch of differential equations, shows 2 stable states (alive or dead) depending on environmental concentrations [2020metabolism]

# computation modeling

models to imitate retina computation [yue2016retinal]
