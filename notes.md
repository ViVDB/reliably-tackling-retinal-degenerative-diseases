# **OLD**

# How to reliably tackle retinal degenerative diseases?

## Main retinal diseases **DONE**

## Treatment options **DONE**

iPSC → epigenetic memory ? (review, medical papers)

## Understanding

→ separate the different retinal diseases
→ need for two kinds of studies:
  * further understanding of the diseases
  * treatment of the disease

    But: treatment through cultured cells needs to be put on hold, as we do not understand enough yet
     Or: treatment needs to be based on reliable monitoring data
         (and it could also profit from in silico models)
         (and it cannot be put in practice before cost and scale by design have been implemented)

→ need different approach from drugs...
→ TE for drug testing... see retina on a chip

is a 1/100 chance to get a cancer in the eye (when you are already blind and it probably won't spread much without notice) unacceptable? Possibility to _cause_ cancer elsewhere?


approche allogénique diminuerait risques

### Animal models

* tomography
	→ materials paper
  * automated image analysis?
* mRNA analysis
	→ materials paper
  * more systematic?
  * in vivo?
* immunohistochemical studies?
	→ materials paper
	→ those are not in vivo, but after dissection
* standardized data collection?
* model validation?
  * negative clinical trials? Diff human/mouse recorded?
  * positive/negative controls?
* sight improvement determination?
* quantum dots for diagnostic?
* safety?
* ophtalmologic tools?
* cell source? newborn mouse is not really usable

### In vitro models

* transcriptomics?
  * more systematic?
* validation?
* photo-sensitive analyses?
  * check cell paper
* neuron connectivity analyses?
  * check cell paper
* automation? reliability?
* in vitro cells assessment ?
  * tomography?
  * individual cups?
  * immunohistochemistry?
  * micro-fluidics?
* standard protocols?
  * "dynamic" protocols based on monitoring...

### In silico models

* need more in vitro models for optic cup generation?
  * need to systematically be studied?
* need for in vivo models?
* need for coupling between both?N. Takata, D. Abbey, L. Fiore, S. Acosta, R. Feng, H. J. Gil, A. Lavado, X. Geng, A. Interiano,
G. Neale, M. Eiraku, Y. Sasai, G. Oliver, An eye organoid approach identifies Six3
suppression of R-spondin 2 as a critical step in mouse neuroretina differentiation.

* need for therapy models...?

### Clinical trial

1/2 of patients with sight improved? see ATMPs paper

## Scaled cell production

## Improve the product itself

## Product delivery to the retina










# OLD

## Review questions

rotating-wall vessel (RWV) ?

where exactly is the OLM barrier?

Müller glia (multiple instances in the text)

# Need to find

more information on surgical procedures → maybe see 98 in review (initial sweep)? 95, 96
	→ reference from the materials paper in the initial sweep
see [MEA]
